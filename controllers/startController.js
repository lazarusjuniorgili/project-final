const { USER, History } = require("../models");

const compChoice = () => {
  const choice = ["batu", "kertas", "gunting"];
  const randomChoice = choice[Math.floor(Math.random() * choice.length)];

  return randomChoice;
};

const findWinner = (p1, comp) => {
  if (p1 === "batu" && comp === "kertas") {
    return "KALAH";
  } else if (p1 === "batu" && comp === "gunting") {
    return "MENANG";
  } else if (p1 === "kertas" && comp === "gunting") {
    return "KALAH";
  } else if (p1 === "kertas" && comp === "batu") {
    return "MENANG";
  } else if (p1 === "gunting" && comp === "kertas") {
    return "MENANG";
  } else if (p1 === "gunting" && comp === "batu") {
    return "KALAH";
  } else {
    return "DRAW";
  }
};

const poinGenerate = (result) => {
  if (result === "MENANG") {
    return 5;
  } else if (result === "DRAW") {
    return 1;
  } else {
    return 0;
  }
};

const startGame = async (reg, res) => {
  const { user_id, game_id, choice } = reg.body;
  const comp = compChoice();
  const gameResult = findWinner(choice, comp);
  const point = poinGenerate(gameResult);

  History.create({
    user_id: user_id,
    game_id: game_id,
    result: gameResult,
    point: point,
  }).then(() => {
    USER.findOne({
      where: {
        id: user_id,
      },
    }).then((data) => {
      USER.update(
        {
          total_point: data.total_point + point,
        },
        {
          where: {
            id: user_id,
          },
        }
      );
    });
    res.status(200).json({
      result: gameResult,
      comp: comp,
      point: point,
    });
  });
};

module.exports = { startGame };
