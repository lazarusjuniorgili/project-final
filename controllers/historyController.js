const { History, USER, GAME } = require("../models");

const getAllHistory = async (reg, res) => {
  try {
    const response = await History.findAll({
      include: [
        {
          model: USER,
          as: "user",
        },
        {
          model: GAME,
          as: "game",
        },
      ],
    });

    res.status(200).json({
      status: "Berhasil mengambil semua history",
      data: response,
    });
  } catch (error) {
    res.status(500).json({
      status: "Gagal mengambil semua history",
    });
  }
};

const getHistoryByUserId = async (reg, res) => {
  try {
    const response = await History.findAll({
      include: [{ model: GAME, as: "game" }],
      where: { user_id: reg.params.userId }
    })

    res.status(200).json({
      status: "berhasil mengambil semua history",
      data: response
    })
  } catch (error) {
    res.status(500).json({
      status: "Gagal data histoy",
      message: error.message
    });
  }
};

module.exports = { getAllHistory, getHistoryByUserId };
