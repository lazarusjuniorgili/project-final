const request = require("supertest");
const app = require("../app");

describe("Auth API", () => {
  test("Post /auth/login --> login user", async () => {
    return request(app)
      .post("/login")
      .send({
        username: "admin189",
        password: "admin189",
      })
      .expect(200);
  });
  test("Post /auth/register --> register user", async () => {
    return request(app)
      .post("/register")
      .send({
        name: "test1",
        username: "test1",
        email: "test1",
        password: "test1",
      })
      .expect(200);
  });
});
