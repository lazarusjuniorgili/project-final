const request = require('supertest');
const app = require('../app');

describe('GAME ROUTER API', () => {
  test('GET /game --> array list game', async () => {
    return request(app)
      .get('/game')
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            status: 'Berhasil mengambil semua data game',
            data: expect.arrayContaining([
              expect.objectContaining({
                id: expect.any(Number),
                name: expect.any(String),
                cover: expect.any(String),
                detail: expect.any(String),
                guide: expect.any(String),
                createdAt: expect.any(String),
                updatedAt: expect.any(String),
              }),
            ]),
          })
        );
      });
  });

  test('GET /game/id --> one object game', () => {
    const id = 1;
    return request(app)
      .get(`/game/${id}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(expect.any(Object));
      });
  });

  test('GET /game/id --> id using sring not allowed', () => {
    const id = 'id-123';
    return request(app)
      .get(`/game/${id}`)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            message: 'Gagal mengambil data game',
          })
        );
      });
  });

  test('GET /game/id --> id not found', () => {
    const id = 666;
    return request(app)
      .get(`/game/${id}`)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            message: 'Berhasil mengambil data game',
            data: null,
          })
        );
      });
  });

  test('POST /game --> add game', () => {
    return request(app)
      .post('/game')
      .send({
        name: 'Rock Scissor Paper',
        cover: 'red-dead.jpg',
        detail: 'description of game',
        guide: 'user guide',
      })
      .expect(200);
  });
});
