const { INTEGER } = require("sequelize");
const request = require("supertest");
const app = require("../app");

describe("User API", () => {
  test("Get /user --> get all user", () => {
    return request(app)
      .get("/user")
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(expect.any(Object));
      });
  });

  test("Get /user/:id --> get one user with id in param", () => {
    return request(app)
      .get("/user/4")
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(expect.any(Object));
      });
  });

  test("Update /user/:id --> update user with id in param", () => {
    return request(app)
      .put("/user/4")
      .send({
        username: "abba",
        email: "abba",
        name: "abba",
      })
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(expect.any(Object));
      });
  });

  test("Update /user/:id --> delete user with id in param", () => {
    return request(app)
      .delete("/user/3")
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(expect.any(Object));
      });
  });
});
