const router = require("express").Router();
const history = require("../controllers/historyController");

router.get("/", history.getAllHistory);
router.get("/:userId", history.getHistoryByUserId);

module.exports = router;
