"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class USER extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = async ({ name, username, email, password }) => {
      const usernameFound = await this.findOne({ where: { username } });
      const emailFound = await this.findOne({ where: { email } });
      if (!usernameFound && !emailFound) {
        const encryptedPassword = this.#encrypt(password);
        const user = await this.create({
          name,
          username,
          email,
          password: encryptedPassword,
        });
        return Promise.resolve(user);
      } else if (usernameFound && emailFound)
        return Promise.reject("Username and Email already used");
      else if (usernameFound) return Promise.reject("Username already used");
      else return Promise.reject("Email already used");
    };
    checkPassword = (password) => bcrypt.compareSync(password, this.password);
    generateToken = () => {
      const payload = { id: this.id, username: this.username };
      const secret = "this is secret";
      const token = jwt.sign(payload, secret);
      return token;
    };

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User not found");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Password invalid");
        else return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  USER.init(
    {
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      total_point: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "USER",
    }
  );
  return USER;
};
